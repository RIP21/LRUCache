import java.util.*;

public class LRUCache<K, V> {

    private static final int DEFAULT_CAPACITY = 100;
    private int capacity;
    private Map<K, V> hashMap;
    private Queue<QueueEntry<K>> queue;


    public LRUCache() {
        this(DEFAULT_CAPACITY);
    }

    public LRUCache(int capacity) {
        this.capacity = capacity;
        hashMap = new HashMap<>(this.capacity);
        queue = new PriorityQueue<>(this.capacity);
    }

    public V put(K key, V value) {
        if (hashMap.containsKey(key)) {
            return refreshRecord(key, value);
        }
        if (hashMap.size() < this.capacity) {
            return add(key, value);
        } else {
            releaseLeastUsed();
            return add(key, value);
        }
    }

    public V get(K key) {
        if (hashMap.containsKey(key)) {
            refreshUsage(key);
        }
        return hashMap.get(key);
    }

    private V refreshRecord(K key, V value) {
        refreshUsage(key);
        return hashMap.put(key, value);
    }

    private V add(K key, V value) {
        addToQueue(key);
        return hashMap.put(key, value);
    }

    private void releaseLeastUsed() {
        hashMap.remove(queue.poll().getKey());
    }

    private void refreshUsage(K key) {
        removeFromQueue(key);
        addToQueue(key);
    }

    private void removeFromQueue(K key) {
        queue.remove(new QueueEntry<>(key));
    }

    private void addToQueue(K key) {
        queue.add(new QueueEntry<>(key));
    }

    public int getCapacity() {
        return this.capacity;
    }

    public int actualSize() {
        return hashMap.size();
    }

    public boolean contains(K key) {
        return hashMap.containsKey(key);
    }

    @Override
    public String toString() {
        return "LRUCache{" +
                "hashMap=" + hashMap +
                '}';
    }

    private class QueueEntry<E> implements Comparable {
        final private E key;
        final private Long timestamp;

        QueueEntry(E key) {
            this.key = key;
            this.timestamp = new Date().getTime();
        }

        private Long getTimestamp() {
            return timestamp;
        }

        private E getKey() {
            return key;
        }

        @Override
        public int compareTo(Object o) {
            QueueEntry entry = (QueueEntry) o;
            return Long.compare(this.timestamp, entry.getTimestamp());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            QueueEntry<?> that = (QueueEntry<?>) o;

            return key != null ? key.equals(that.key) : that.key == null;

        }

        @Override
        public int hashCode() {
            return key != null ? key.hashCode() : 0;
        }
    }
}

