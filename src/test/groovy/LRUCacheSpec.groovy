import spock.lang.Specification

class LRUCacheSpec extends Specification {


    def "Have initial size of 100 in empty constructor"() {
        when:
            LRUCache lruCache = new LRUCache();
        then:
            lruCache.getCapacity() == 100
    }

    def "Have capacity set from constructor"() {
        when:
            LRUCache lruCache = new LRUCache(20);
        then:
            lruCache.getCapacity() == 20
    }

    def "Put and return correct value by key"() {
        setup:
            LRUCache lruCache = new LRUCache();
        when:
            def expectedValue = "Expected"
            lruCache.put(1, expectedValue)
            def result = lruCache.get(1)
        then:
            result == expectedValue
    }

    def "Return actual size of the cache"() {
        setup:
            LRUCache lruCache = new LRUCache();
        when:
            lruCache.put(1, "Expected")
            def result = lruCache.actualSize()
        then:
            result == 1
        when:
            lruCache.put(2, "String")
            result = lruCache.actualSize()
        then:

            result == 2
    }

    def "Cache contains no more elements than provided capacity"() {
        setup:
            LRUCache lruCache = new LRUCache(2);
        when:
            lruCache.put(1, "Expected")
            lruCache.put(2, "Expected")
            lruCache.put(3, "Expected")
        then:
            lruCache.actualSize() == 2;
    }

    def "Least used elements is removing from the cache"() {
        setup:
            LRUCache lruCache = new LRUCache(4);
            def initialElements = [1: "Expected1",
                                   2: "Expected2",
                                   3: "Expected3",
                                   4: "Expected4"]
        when: "Putting all elements till capacity max"
            initialElements.each { element ->
                lruCache.put(element.key, element.value)
            }
        then: "Should contain all elements"
            initialElements.each { element ->
                lruCache.contains(element)
            }

        when: "Put additional unique value should push least used element out of the cache"
            lruCache.put(5, "Expected5")
        then: "Since Expected1 was firstly putted in, it is least used so must be removed"
            !lruCache.contains(1)
    }

    def "Random elements adding"() {
        setup:
            LRUCache lruCache = new LRUCache(4);
            def initialElements = [1: "Expected1",
                                   2: "Expected2",
                                   3: "Expected3",
                                   4: "Expected4"]
        when: "Putting all elements till capacity max"
            initialElements.each { element ->
                lruCache.put(element.key, element.value)
            }
            initialElements.each { element ->
                lruCache.put(element.key, element.value)
            }
        then: "Should contain all elements"
            initialElements.each { element ->
                lruCache.contains(element)
            }
        when: "Put additional unique value should push least used element out of the cache"
            lruCache.put(5, "Expected5")
            lruCache.put(5, "Expected5")
            lruCache.put(4, "Expected4")
            lruCache.put(4, "Expected4")
            lruCache.put(6, "Expected6")
            lruCache.put(7, "Expected7")
            lruCache.put(8, "Expected8")
        then:
            lruCache.actualSize() == 4
            lruCache.queue.size() == 4
            !lruCache.contains(1)
            !lruCache.contains(2)
            !lruCache.contains(3)
    }

    def "If element was get and element is not on hashMap queue stays empty"() {
        when:
            LRUCache lruCache = new LRUCache(20)
            lruCache.get(1)
        then:
            lruCache.queue.size() == 0;
    }

    def "If element was added and then added again with different value, should be overridden"() {
        setup:
            LRUCache lruCache = new LRUCache(20)
        when:
            lruCache.put(1, "Test1")
        then:
            lruCache.get(1) == "Test1"
        when:
            lruCache.put(1, "Test2")
        then:
            lruCache.get(1) == "Test2"
    }


}
